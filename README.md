# Docker + October CMS

Dockerfile a docker-compose.yml pro testování vytváření náhodných obrázků. 
Port na kterém poté aplikace běží je nastav v "docker-compose.yml" na 8080.

1) Build default image: 
    
`docker build -t docker-master .    ` 


2) Vytvoření volumes:

`   docker volume create volumen-october-docker-storage  `

3) Spuštění docker-compose

`docker-compose up  `

4) Administrace se nachází na adrese {$server}/backend
-    Jméno: admin
-    Heslo: admin

5) V administraci máš v horním menu záložku Images a poté v gridu je tlačítko Vygenerovat nové záznamy. 
Tím se ti spustí generování náhodných obrázků. Zapisují se na disk i do databáze. 
Počet generovaných obrázků nastavíš v záložce Settings -> Sekce Docket Test - Images (Základní nastavení)